import os
import io
import sys
import string
import shutil

#Get the name of the project for config descriptions
path = os.getcwd()
parentdir = (os.path.abspath(os.path.join(path, os.pardir)))
parentdir = (parentdir.rsplit('/',1))[1]
parentdir = (parentdir.replace('_',' ')).title()

modelList = []
modelDict = {}
templatedir = "/mnt/c/Users/Steam/ros"
rangePrefix = "range "
desiredExts = [".dae", ".stl", ".obj", ".mtl"]

def main():
    #Check if template directory exists
    if not(os.path.exists(templatedir)):
        print("\033[41mERROR: The path given for the templates could not be found.\033[0m\nPlease change the templatedir string to a valid directory.")
        print("\033[41mABORTING...\033[0m")
        sys.exit()

    fileList = os.listdir(path)

    #Workable model name list creation
    for i in range(0, len(fileList)):
        for j in range(0, len(desiredExts)):
            if fileList[i].endswith(desiredExts[j]):
                global modelList
                global modelDict
                tempModel = fileList[i].replace(desiredExts[j], "", 1)
                modelList.append(tempModel)
                modelDict[tempModel] = desiredExts[j]

    #Removing duplicates
    modelList = list(set(modelList))

    #Directory and file creation
    for i in range(0,len(modelList)):
        modelPath = str(path) + "/" + modelList[i]
        modelExt = modelDict[modelList[i]]
        meshesPath = modelPath + "/meshes/"
        os.makedirs(modelPath)
        os.chdir(modelPath)
        os.mkdir("meshes")

        templatedirSDF = str(templatedir) + "/" + "tempSDF.txt"
        tempSDF = io.open(templatedirSDF, "r")
        emptySDF = io.open("model.sdf", "w")
        replaceLine(tempSDF, emptySDF, modelList[i], modelExt)

        templatedirCONFIG = str(templatedir) + "/" + "tempCONFIG.txt"
        tempCONFIG = open(templatedirCONFIG, "r")
        emptyCONFIG = io.open("model.config", "w")
        replaceLine(tempCONFIG, emptyCONFIG, modelList[i], modelExt)

        #Moving meshes to new directories
        shutil.move((modelPath + modelExt), (meshesPath + modelList[i] + modelExt))
        if modelExt == ".obj":  #Special case for supplementary .mtl files
            shutil.move((modelPath + ".mtl"), (meshesPath + modelList[i] + ".mtl"))

        os.chdir(path)

def removePrefix(modelname, prefix):
    resName = modelname.replace(prefix, "", 1)
    return resName

def replaceLine(tempFile, emptyFile, currentModel, currentModel_ext):
    for line in tempFile:
        line = line.replace("$modelName", currentModel)
        line = line.replace("$parent_directory", parentdir)
        line = line.replace("$extension", currentModel_ext)
        emptyFile.write(line.decode('utf-8'))
    emptyFile.close()

if __name__ == "__main__":
    main()
